import os
import subprocess
import asyncio
import click
import json
from pprint import pprint
from datetime import datetime
# from xml.etree import ElementTree as ET
from lxml import etree as ET

class Response(object):
    def __init__(self, *args, **kwargs):
        for k, v in kwargs.items():
            self.__setattr__(k, v)

class Rss(object):
    ns = {'itunes': 'http://www.itunes.com/dtds/podcast-1.0.dtd'}
    ET.register_namespace('itunes', 'http://www.itunes.com/dtds/podcast-1.0.dtd')
    web_root = 'https://jagabouz.com/radio'
    description = 'jagabouz private radio archive'

    def __init__(self, prog, output_path, prog_path, audio_file):
        self.prog = prog
        self.rss_path = '%s/%s/rss.xml' % (output_path, prog_path)
        self.output_path = output_path
        self.prog_path = prog_path
        self.audio_file  = audio_file
        print('\n\nRss is runnning...')

    def __pretty_ft(self, f):
        print('__pretty_ft')
        ft = self.prog.attrib['ft']
        print('ft: %s' % ft)
        d = datetime.strptime(ft, '%Y%m%d%H%M%S')
        return datetime.strftime(d, f)

    def __now_pretty(self):
        f = '%a %d %b %Y %H:%M:%S %z'
        JST = timezone(timedelta(hours=+9), 'JST')
        return datetime.strftime(datetime.now(JST), f)

    def __create_new_rss(self):
        try:
            parser = ET.XMLParser(remove_blank_text=True)
            new_rss = ET.parse('./assets/base_rss.xml', parser)
            root = new_rss.getroot()
            root.find('.//title').text = self.prog.find('.//title').text
            root.find('.//description').text = '%s: %s' % (self.description, self.prog.find('.//title').text)
            root.find('.//link').text = self.web_root
            root.find('.//language').text = 'ja_JP'
            root.find('.//itunes:image', self.ns).set('href', '%s/%s/cover.jpg' % (self.web_root, self.prog_path))
            return new_rss
        except Exception as e: print(e)

    def __get_rss(self):
        if not os.path.exists(self.rss_path):
            return self.__create_new_rss()

        parser = ET.XMLParser(remove_blank_text=True)
        return ET.parse(self.rss_path, parser)

    def __append_item(self):
        audio_url = '%s/%s/%s' % (self.web_root, self.prog_path, self.audio_file)
        item = ET.Element('item')
        try:
            ET.SubElement(item, 'title').text = self.__pretty_ft('%Y.%m.%d')
            ET.SubElement(item, 'link').text = self.web_root
            ET.SubElement(item, 'guid').text = audio_url
            ET.SubElement(item, 'description').text = self.prog.find('.//desc').text

            # making <itunes:summary><![CDATA[---]]></itunes:summary>
            summary = ET.SubElement(item, ET.QName(self.ns['itunes'], 'summary'))
            cdata = ET.CDATA(self.prog.find('.//info').text)
            summary.text = cdata

            # make enclosure tag
            enclosure = ET.SubElement(item, 'enclosure')
            enclosure.set('url', audio_url)
            enclosure.set('length', '0')
            enclosure.set('type', 'audio/aac')

            ET.SubElement(item, 'category').text = 'Podcast'
            ET.SubElement(item, 'pubDate').text = self.__pretty_ft('%a %d %b %Y %H:%M:%S +0900')

            rss = self.__get_rss()
            channel = rss.getroot().find('channel').append(item)
            return rss
        except Exception as e: print(e)

    def update(self):
        rss = self.__append_item()
        rss.getroot().find('.//pubDate').text = self.__now_pretty()
        if os.path.exists(self.rss_path):
            bk_path = self.rss_path + '.bk'
            print('rss file is already exists. Make backup file.\n\tOriginal:\t%s\n\tBackup:\t%s' % (self.rss_path, bk_path))
            try:
                subprocess.call('mv -f %s %s' % (self.rss_path, bk_path), shell=True)
            except Exception as e: print(e)

        print('Create new rss file: %s' % self.rss_path)
        try:
            rssStr = ET.tostring(rss.getroot(), encoding='utf-8', pretty_print=True)
            with open(self.rss_path, "wb") as f:
                f.write(rssStr)
        except Exception as e: print(e)
