# radipy
Radipy is CLI radiko Downloader written by python3.

# Usage

```
python radipy.py --help
Usage: radipy.py [OPTIONS]

  Radipy is CLI radiko Downloader written by python3.

Options:
  -a, --area  print station id & name in your area
  -id TEXT    set station id
  -ft TEXT    set start time
  --clear     clear authkey and player in tmp dir
  --help      Show this message and exit.
```

# requirements

```
pip install -r requirements.txt
```

```
brew install ffmpeg swfextract
```

# Tested

+ MacOS 10.11.6
+ Python 3.5.2
+ pip 9.0.1
+ Homebrew 1.1.11
