import os
from datetime import datetime, timezone, timedelta
import subprocess
import sys
import base64
import asyncio
import requests
import click
from prettytable import PrettyTable
from xml.etree import ElementTree as ET
from spinner import Spinner
from pprint import pprint
from rss import Rss

root_path = os.path.dirname(os.path.abspath(__file__))

class Response(object):
    def __init__(self, *args, **kwargs):
        for k, v in kwargs.items():
            self.__setattr__(k, v)


class Radipy(object):
    player_url = 'http://radiko.jp/apps/js/flash/myplayer-release.swf'
    fms1_url = 'https://radiko.jp/v2/api/auth1_fms'
    fms2_url = 'https://radiko.jp/v2/api/auth2_fms'
    LANG = 'ja_JP.utf8'
    JST = timezone(timedelta(hours=+9), 'JST')
    date = datetime.strftime(datetime.now(JST), '%Y-%m-%d-%H')
    tmp_path='%s/tmp' % root_path
    playerfile='%s/player.%s.swf' % (tmp_path, date)
    keyfile = '%s/authkey.%s.jpg' % (tmp_path, date)
    playlistfile = '%s/playlist.%s.m3u8' % (tmp_path, date)
    auth_response = Response()
    auth_success_response = Response()
    output_path = '%s/output' % root_path
    program_data = Response()

    def __init__(self, station_id, ft):
        self.station_id = station_id
        self.ft = ft
        partialkey = ''
        self.stream_url = ''
        self.area_id = ''
        self.title = ''

    @staticmethod
    def ft(s, e):
        print('s: %s' % s)
        print('e: %s' % e)
        day = datetime.datetime.today()
        if s > e:
            day += datetime.timedelta(days=-1)

        return datetime.datetime.strftime(day, '%Y%m%d') + s

    @staticmethod
    def clear():
        subprocess.call('rm -vr %s' % tmp_path, shell=True)

    def authenticate(self):
        self._get_playerfile()
        self._get_keyfile()
        self._get_auth1()
        self._generate_particlekey()
        self._get_auth2()
        print('--------------------------')
        print('authentication success.')

    def get_channels(self):
        self.authenticate()
        self._get_area_id()
        self._get_area_channels()

    def create(self):
        self.authenticate()
        self._get_area_id()
        self._get_stream_url()
        spinner = Spinner()
        sys.stdout.write("Now Downloading...")
        spinner.start()
        if self._create_aac():
            sys.stdout.write('finish!!')
        else:
            sys.stdout.write('failed!!')
        spinner.stop()

    def update_rss(self):
        audio_file = '%s_%s.aac' % (self.title, self.ft[:8])
        rss = Rss(self.prog, self.output_path, self.title, audio_file)
        rss.update()

    def _get_playerfile(self):
        if not os.path.exists(self.tmp_path):
            subprocess.call('mkdir {}'.format(self.tmp_path), shell=True)
        if not os.path.exists(self.playerfile):
            print('create playerFile...')
            res = requests.get(self.player_url)
            if res.status_code == 200:
                with open(self.playerfile, 'wb') as file:
                    file.write(res.content)
                if not os.path.exists(self.playerfile):
                    print('PlayerFile is not created.')

    def _get_keyfile(self):
        if not os.path.exists(self.tmp_path):
            subprocess.call('mkdir {}'.format(self.tmp_path), shell=True)
        if not os.path.exists(self.keyfile):
            print('create KeyFile...')
            subprocess.call('swfextract -b 12 {} -o {}'.format(self.playerfile, self.keyfile), shell=True)
            if not os.path.exists(self.keyfile):
                print('Keyfile is not created.')

    def _get_auth1(self):
        print('access auth1_fms...')
        headers = {
            'Host': 'radiko.jp',
            'pragma': 'no-cache',
            'X-Radiko-App': 'pc_ts',
            'X-Radiko-App-Version': '4.0.0',
            'X-Radiko-User': 'test-stream',
            'X-Radiko-Device': 'pc'
        }
        res = requests.post(url=self.fms1_url, headers=headers)
        self.auth_response.body = res.text
        self.auth_response.headers = res.headers
        self.auth_response.authtoken = self.auth_response.headers['x-radiko-authtoken']
        self.auth_response.offset = int(self.auth_response.headers['x-radiko-keyoffset'])
        self.auth_response.length = int(self.auth_response.headers['x-radiko-keylength'])

    def _generate_particlekey(self):
        print('generate particleKey...')
        f = open(self.keyfile, 'rb+')
        f.seek(self.auth_response.offset)
        data = f.read(self.auth_response.length)
        self.partialkey = base64.b64encode(data)

    def _get_auth2(self):
        print('access auth2_fms...')
        headers ={
          'pragma': 'no-cache',
          'X-Radiko-App': 'pc_ts',
          'X-Radiko-App-Version': '4.0.0',
          'X-Radiko-User': 'test-stream',
          'X-Radiko-Device': 'pc',
          'X-Radiko-Authtoken': self.auth_response.authtoken,
          'X-Radiko-Partialkey': self.partialkey,
        }
        res = requests.post(url=self.fms2_url, headers=headers)
        self.auth_success_response.body = res.text
        self.auth_success_response.headers = res.headers

    def _get_area_id(self):
        area = self.auth_success_response.body.strip().split(',')
        self.area_id = area[0]
        print('area_id: %s' % self.area_id)

    def _get_area_channels(self):
        area_api_url = "http://radiko.jp/v2/api/program/today"
        params = {'area_id': self.area_id}
        res = requests.get(url=area_api_url, params=params)
        channels_xml = res.content
        tree = ET.fromstring(channels_xml)
        channels = tree.findall('.//station')
        table = PrettyTable(['id', '名前'])
        table.align['id'] = 'l'
        table.align['名前'] = 'l'
        table.padding_width = 2
        for channel in channels:
            table.add_row([channel.attrib['id'], channel.find('name').text])
        print(table)

    def _get_stream_url(self):
        try:
            datetime_api_url = 'http://radiko.jp/v3/program/date/{}/{}.xml'.format(self.ft[:8], self.area_id)
            res = requests.get(url=datetime_api_url)
            channels_xml = res.content
            tree = ET.fromstring(channels_xml)
            station = tree.find('.//station[@id="{}"]'.format(self.station_id))
            prog = station.find('.//prog[@ft="{}"]'.format(self.ft))
            to = prog.attrib['to']

        # 日を跨いでいる場合は前の日の番組表を探す
        except AttributeError:
            datetime_api_url = 'http://radiko.jp/v3/program/date/{}/{}.xml'.format(int(self.ft[:8]) - 1, self.area_id)
            res = requests.get(url=datetime_api_url)
            channels_xml = res.content
            tree = ET.fromstring(channels_xml)
            station = tree.find('.//station[@id="{}"]'.format(self.station_id))
            prog = station.find('.//prog[@ft="{}"]'.format(self.ft))
            to = prog.attrib['to']

        self.title = prog.find('.//title').text.replace(' ', '_').replace('　', '_').replace("'", "")
        table = PrettyTable(['title'])
        table.add_row([self.title])
        table.padding_width = 2
        print(table)
        self.prog = prog
        self.stream_url = 'https://radiko.jp/v2/api/ts/playlist.m3u8?l=15&station_id={}&ft={}&to={}'.format(
            self.station_id,
            self.ft,
            to
        )

    def _create_aac(self):
        try:
            if not os.path.exists('%s%s' % (self.output_path, self.title)):
                subprocess.call('mkdir -p {}/{}'.format(
                    self.output_path, self.title), shell=True)
            cmd = ('ffmpeg '
                   '-loglevel fatal '
                   '-n -headers "X-Radiko-AuthToken: {}" '
                   '-i "{}" '
                   '-vn -acodec copy "{}/{}/{}.aac"'.format(
                    self.auth_response.authtoken,
                    self.stream_url,
                    self.output_path,
                    self.title,
                    '{}_{}'.format(self.title, self.ft[:8])
                    ))
            subprocess.call(cmd, shell=True)
            print('{}/{}/{}.aac'.format(self.output_path, self.title, '{}_{}'.format(self.title, self.ft[:8])))
            return True
        except Exception as e:
            print(e);
            self.clear()
            return False


@click.command(help='Radipy is CLI radiko Downloader written by python3.')
@click.option('-a', '--area', is_flag=True, help='print station id & name in your area')
@click.option('-id', type=str, help='set station id')
@click.option('-ft', type=str, help='set start time')
@click.option('-s', type=str, help='set start time')
@click.option('-e', type=str, help='set end time')
@click.option('--rss', is_flag=True, help='update rss file')
@click.option('--clear', is_flag=True, help='clear authkey and player in tmp dir')
def main(area, rss, id, ft, clear, s, e):
    if clear:
        Radipy.clear()
    if area:
        radipy = Radipy(0, 0)
        radipy.get_channels()
    if s and e:
        ft = Radipy.ft(s=s, e=e)
        print('ft: %s' % ft)
    if id and ft:
        radipy = Radipy(station_id=id, ft=ft)
        radipy.create()
        if rss:
            radipy.update_rss()

if __name__ == '__main__':
    main()
